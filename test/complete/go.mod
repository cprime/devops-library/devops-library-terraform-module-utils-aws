module gitlab.com/cprime/devops-library/devops-library-terraform-module-utils-aws

go 1.3

require (
	github.com/agext/levenshtein v1.2.3 // indirect
	github.com/gruntwork-io/terratest v0.35.6
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/hashicorp/hcl/v2 v2.10.0 // indirect
	github.com/hashicorp/terraform-json v0.11.0 // indirect
	github.com/jinzhu/copier v0.3.2 // indirect
	github.com/mitchellh/go-wordwrap v1.0.1 // indirect
	github.com/stretchr/testify v1.7.0
	github.com/zclconf/go-cty v1.8.3 // indirect
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a // indirect
	golang.org/x/net v0.0.0-20210614182718-04defd469f4e // indirect
	golang.org/x/sys v0.0.0-20210616094352-59db8d763f22 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
